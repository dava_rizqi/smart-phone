<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use File;

class UserController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::get();
        return view('admin.users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.users.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = \Validator::make($request->all(),[
            "name" => "required|max:100",
            "username" => "required|max:20|unique:users",
            "roles" => "required",
            "phone" => "required|digits_between:10,12",
            "address" => "required|max:200",
            "avatar" => "required",
            "email" => "required|email|unique:users",
            "password" => "required",
            "password_confirmation" => "required|same:password"
        ])->validate();
        
        $new_user = new \App\User;
        $new_user->name = $request->get('name');
        $new_user->username = $request->get('username');
        $new_user->role = $request->get('roles');
        $new_user->address = $request->get('address');
        $new_user->phone = $request->get('phone');
        $new_user->email = $request->get('email');
        $new_user->password = \Hash::make($request->get('password'));

        if($request->file('avatar')){
            $fileGet = $request->avatar;
            $fileName = Str::random(30).'.'.$fileGet->getClientOriginalExtension();
            //dd($fileGet, $fileName);
            $move = $fileGet->move('uploads/avatars', $fileName);
            $new_user->avatar = $fileName;
        }

        $new_user->save();
        
        return redirect()->route('users.create')->with('status', 'User successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.users.edit', ['user' => $user]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::findOrFail($id);

        return view('admin.users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \App\User::findOrFail($id);

        $user->name = $request->get('name');
        $user->id_roles = $request->get('roles');
        // $user->address = $request->get('address');
        // $user->phone = $request->get('phone');

        // if($request->file('avatar')){
        //     if($user->avatar && File::exists('uploads/avatars/' . $user->avatar)){
        //         File::delete('uploads/avatars/' . $user->avatar);
        //         $fileGet = $request->avatar;
        //         $fileName = Str::random(30).'.'.$fileGet->getClientOriginalExtension();
        //         $move = $fileGet->move('uploads/avatars', $fileName);
        //         $user->avatar = $fileName;
        //     }
        //     else{
        //         $fileGet = $request->avatar;
        //         $fileName = Str::random(30).'.'.$fileGet->getClientOriginalExtension();
        //         $move = $fileGet->move('uploads/users', $fileName);
        //         $user->avatar = $fileName;
        //     }
        // }

        $user->save();

        return redirect()->route('users.index')->with('status', 'User succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $user = \App\User::findOrFail($id);

        File::delete('uploads/avatars/' . $user->avatar);
        $user->delete();

        return redirect()->route('users.index')->with('status', 'User successfully deleted');
    }
}
