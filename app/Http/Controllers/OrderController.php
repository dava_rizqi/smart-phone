<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = \App\Order::leftJoin('users','orders.user_id','users.id')
                        ->leftJoin('user_profiles','orders.user_id','user_profiles.id_users')
                        // ->leftJoin('order_items','orders.id','order_items.order_id')
                        ->select('orders.*','user_profiles.city', 'user_profiles.address','user_profiles.postal','user_profiles.phone', 'users.name', 'users.email')
                        ->get(); 

        return view('admin.orders.index', $orders);
    }
    public function searchAddress(Request $request){
        
        $orders = \App\Order::leftJoin('users','orders.user_id','users.id')
                        ->leftJoin('user_profiles','orders.user_id','user_profiles.id_users')
                        // ->leftJoin('order_items','orders.id','order_items.order_id')
                        ->select('orders.*', 'orders.address as oad','user_profiles.city', 'user_profiles.address','user_profiles.postal','user_profiles.phone', 'users.name', 'users.email')->where('users.name' , 'like' , '%'.$request->search.'%')
                        ->get(); 

        // $orders = \App\Order::get();

        return $orders;
    }
    public function getAllOrder()
    {
        $orders = \App\Order::leftJoin('users','orders.user_id','users.id')
                        ->leftJoin('user_profiles','orders.user_id','user_profiles.id_users')
                        // ->leftJoin('order_items','orders.id','order_items.order_id')
                        ->select('orders.*', 'orders.address as oad' ,'user_profiles.city', 'user_profiles.address','user_profiles.postal','user_profiles.phone', 'users.name', 'users.email')
                        ->get(); 

        // $orders = \App\Order::get();

        return $orders;
    } 

    public function orderDetail($id)
    {
        $detailOrders = \App\OrderItems::leftJoin('products','order_items.product_id','products.id')
                        ->select('products.*', 'order_items.*')
                        ->where('order_id', $id)->get();

        return $detailOrders;
    }

    public function confirm($id)
    {   
        $data = \App\Order::find($id);
        $data->status = 1;
        $data->save();
        return $data->status;
    }

    public function send($id)
    {   
        $data = \App\Order::find($id);
        $data->status = 2;
        $data->save();
        return $data->status;
    }

    public function done($id)
    {   
        $data = \App\Order::find($id);
        $data->status = 3;
        $data->save();
        return $data->status;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $order = \App\Order::findOrFail($id);
        $order->delete();
    }
}
