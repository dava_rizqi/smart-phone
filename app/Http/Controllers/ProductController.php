<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
class ProductController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = \App\Product::get();
        return view('admin.products.index', ['products' => $products]);
    }
    public function filterCondition(Request $request){
        
        $products = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->where('products.product_condition', $request->product_condition )->get();
        return $products;
    }
    public function searchCategory(Request $request){
        $products = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->where('products.product_category', $request->id )->get();
        return $products;
    }

    public function all()
    {
        $products = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->get();
        return $products;
    }
    public function allPaginate()
    {
        $products = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->inRandomOrder()->limit(8)->get();
        return $products;
    }
    public function search(Request $request)
    {
        $products = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->where('product_name' , 'like' , '%'. $request->search .'%')->get();
        return $products;
    }

    public function data($id){
        $product = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->where('products.id', $id)->first();
        return $product;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = \App\Category::get();

        return view("admin.products.create", $data); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_product = new \App\Product;
        $new_product->product_category = $request->product_category;
        $new_product->product_condition = $request->product_condition;
        $new_product->product_name = $request->product_name;
        $new_product->product_description = $request->product_description;
        $new_product->product_price = $request->product_price;
        $new_product->product_stock = $request->product_stock;
        $images = [];
        if($request->hasFile('files')){
            foreach ($request->file('files') as $key) {
                
                $fileName = Str::random(8) . '.' . $key->getClientOriginalExtension();
                $move = $key->move('uploads/product_images', $fileName);
                array_push($images, $fileName);
            }
            $image = implode(',' , $images);
            $new_product->product_image = $image;
        }else{
            return 'ga ada';
        }
            

        $new_product->save();
        return $request->file('file');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['product'] = \App\Product::findOrFail($id);
        $data['categories'] = \App\Category::get();

        return view('admin.products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $images = [];
        $product = \App\Product::findOrFail($id);

        $product->product_category = $request->get('product_category');
        $product->product_condition = $request->product_condition;
        $product->product_name = $request->get('product_name');
        $product->product_description = $request->get('product_description');
        $product->product_stock = $request->get('product_stock');
        $product->product_price = $request->get('product_price');
        if($request->file('product_image')){
            // dd($request->all());    
            foreach ($request->file('product_image') as $key) {
                // dd($key->extension());
                $fileName = Str::random(8) . '.' . $key->getClientOriginalExtension();
                $move = $key->move('uploads/product_images', $fileName);
                array_push($images, $fileName);
            }
            $image = implode(',' , $images);
            $product->product_image = $image;
        }

        $product->save();

        return redirect()->route('products.index')->with('status', 'Product successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $product = \App\Product::findOrFail($id);
        // File::delete('uploads/product_images/' . $product->product_images);
        $product->delete();
    }
}
