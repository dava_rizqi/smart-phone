<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function all()
    {
        $products = \App\Product::leftJoin('categories', "categories.id", "products.product_category")->select('products.*', 'categories.category_name')->get();
        return $products;
    }
}
