<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimony;
use Illuminate\Support\Str;
class TestimonyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimony = \App\Testimony::get();
        return view('admin.testimonies.index', ['testimonies' => $testimony]);
    }
    public function all()
    {
        $testimony = \App\Testimony::leftJoin('products', "testimonies.product_id", "products.id")->select('products.product_name', 'testimonies.*')->get();
        return $testimony;
    }
    public function allPublish()
    {
        $testimony = \App\Testimony::leftJoin('products', "testimonies.product_id", "products.id")->select('products.product_name', 'testimonies.*')->where('is_publish' , true)->get();
        return $testimony;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['products'] = \App\Product::all();

        return view("admin.testimonies.create", $data); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testimony = new \App\Testimony();
        $testimony->product_id = $request->product_id;
        $testimony->name = $request->name;
        $testimony->is_publish = $request->is_publish;
        $testimony->testimony = $request->testimony;
        $images = [];
        if($request->hasFile('files')){
            foreach ($request->file('files') as $key) {
                
                $fileName = Str::random(8) . '.' . $key->getClientOriginalExtension();
                $move = $key->move('uploads/testimony_images', $fileName);
                array_push($images, $fileName);
            }
            $image = implode(',' , $images);
            $testimony->image = $image;
        }else{
            return 'ga ada';
        }
            

        $testimony->save();
        return $request->file('file');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['testimony'] = \App\Testimony::findOrFail($id);
        $data['products'] = \App\Product::get();

        return view('admin.testimonies.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $images = [];
        $testimony = \App\Testimony::findOrFail($id);

        $testimony->product_id = $request->get('product_id');
        $testimony->name = $request->name;
        $testimony->testimony = $request->get('testimony');
        $testimony->is_publish = $request->get('is_publish');
        if($request->file('image')){
            // dd($request->all());    
            foreach ($request->file('image') as $key) {
                // dd($key->extension());
                $fileName = Str::random(8) . '.' . $key->getClientOriginalExtension();
                $move = $key->move('uploads/testimony_images', $fileName);
                array_push($images, $fileName);
            }
            $image = implode(',' , $images);
            $testimony->image = $image;
        }

        $testimony->save();

        return redirect()->route('testimonies.index')->with('status', 'Product successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $testimony = \App\Testimony::findOrFail($id);
        // File::delete('uploads/testimony_images/' . $testimony->product_images);
        $testimony->delete();
    }
}
