<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    //
    protected $fillable = ([
        'address', 'id_users', 'city', 'postal',
    ]);
}
