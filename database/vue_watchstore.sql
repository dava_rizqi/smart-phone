-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2020 at 10:25 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vue_watchstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_slug` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_slug`, `category_image`, `created_at`, `updated_at`) VALUES
(1, 'WANITA', 'wanita', NULL, '2020-01-09 19:30:07', '2020-01-09 19:30:07'),
(2, 'PRIA', 'pria', NULL, '2020-01-09 19:30:17', '2020-01-09 19:30:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_07_122845_create_oauth_providers_table', 1),
(4, '2018_03_20_192027_create_orders_table', 1),
(5, '2018_03_20_192950_create_order_items_table', 1),
(6, '2019_10_08_033039_create_user_profiles_table', 1),
(7, '2019_10_10_114613_create_products_table', 1),
(8, '2019_10_10_123233_create_roles_table', 1),
(9, '2019_10_10_125522_create_brands_table', 1),
(10, '2019_10_10_125601_create_categories_table', 1),
(11, '2019_10_14_012810_create_data_table', 1),
(12, '2019_10_16_123850_create_types_table', 1),
(13, '2019_11_05_222052_create_book_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_providers`
--

CREATE TABLE `oauth_providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refresh_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `total` int(7) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `date`, `address`, `payment`, `status`, `total`, `created_at`, `updated_at`) VALUES
(2, 7, '2020-01-11 00:07:14', ', , ', 'MySQL.png', 1, 8585000, '2020-01-10 17:03:19', '2020-01-10 17:07:14'),
(3, 9, '2020-01-11 06:29:29', ', , ', NULL, 0, 6840000, '2020-01-10 23:29:29', '2020-01-10 23:29:29');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 5700000, '2020-01-10 17:02:54', '2020-01-10 17:02:54'),
(2, 1, 4, 1, 2885000, '2020-01-10 17:02:54', '2020-01-10 17:02:54'),
(3, 2, 1, 2, 5700000, '2020-01-10 17:03:19', '2020-01-10 17:03:19'),
(4, 2, 4, 1, 2885000, '2020-01-10 17:03:19', '2020-01-10 17:03:19'),
(5, 3, 3, 2, 6840000, '2020-01-10 23:29:29', '2020-01-10 23:29:29');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_category` int(11) DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_stock` int(11) DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_category`, `product_name`, `product_image`, `product_description`, `product_stock`, `product_price`, `created_at`, `updated_at`) VALUES
(1, 1, 'MARC JACOBS - Henry Black Leather', 'MJ1644.png', 'Simple & Chic merupakan karakteristik jam tangan wanita Marc Jacobs Henry ini. Dengan sunray dial, tulisan MARC pad dial, serta strap kulit hitam membuat jam tangan ini sangat versatile.', 135, 2850000, '2020-01-09 19:40:58', '2020-01-09 19:40:58'),
(2, 1, 'MICHAEL KORS – Portia Gold Stainless', 'MK3788.png', 'Dengan inspirasi desain minimalis dan modern, jam tangan Michael Kors Portia cocok untuk wanita urban yang elegan. Ia dilengkapi dengan dial hitam yang kontras dan subdial detil berkristal.', 99, 3420000, '2020-01-09 19:44:30', '2020-01-09 19:44:30'),
(3, 1, 'MICHAEL KORS – Portia Rose Gold Stainless', 'MK3839.png', 'Dengan stainless steel case dan dial minimalis, jam tangan Michael Kors portia merupakan salah satu jam tangan wanita klasik yang kami miliki. Dengan H link bracelet dan aksen Kristal pada subdia menjadikannya chic.', 129, 3420000, '2020-01-09 19:45:24', '2020-01-09 19:45:24'),
(4, 1, 'FOSSIL – Neely Pastel Pink Stainless Steel', 'ES4404.png', 'Dial pada jam tangan Neely terbuat dari marmer asli berwarna putih dengan bazel berwarna pink pastel, memiliki three hand movement dan strap berbahan mesh stainless steel.', 86, 2885000, '2020-01-09 19:46:19', '2020-01-09 19:46:19'),
(5, 1, 'KATE SPADE – Metro Black Leather', 'KSW1469.png', 'Jam tangan Metro Black hadir dengan desain strap half bangle yang chic dan simple terbuat dari material kulit asli, dikombinasukan dengan case berbahan stainless stell serta dial yang terbuat dari mother of pearl yang memberikan kesan mewah pada jam tangan ini.', 110, 3365000, '2020-01-09 19:47:21', '2020-01-09 19:47:21'),
(11, 2, 'FOSSIL -  Townsman Chronograph Brown Leather (FS5437)', 'jam6.png', 'Memiliki dial berwarna coklat dikombinasikan dengan bazel berwarna hitam serta strap berbahan kulit asli berwarna coklat semakin menambah kesan maskulin dan classic untuk penggunanya', 94, 2885000, '2020-01-09 21:40:34', '2020-01-09 21:40:34'),
(12, 2, 'EMPORIO ARMANI – Men’s Retro Silver Stainless Black Dial  (AR1676)', 'jam7.png', 'Retro dan refined dengan 5 link stainless steel bracelet case bersubdial', 55, 4029000, '2020-01-09 21:41:19', '2020-01-09 21:41:19'),
(13, 2, 'DIESEL – Rasp Black IP and Leather Three Hand Watch (DZ1841)', 'jam8.png', 'Pakai evolusi desain terbaru dari Diesel. Jam tangan original pria Diesel Rasp hadir dengan tali kulit dan dial berwarna hitam yang maskulin, kasual, dan cocok untuk digunakan sehari hari.', 78, 2650000, '2020-01-09 21:42:30', '2020-01-09 21:42:30'),
(14, 2, 'DIESEL – Boltdown Chronograph Gray Silicone (DZ7416)', 'jam9.png', 'Memiliki tampilan dan fungsi yang baik menjadikan jam tangan ini adalah pilihan sempurna. Fitur pada dial yang dapat menampilkan tiga waktu yang berbeda, serta dikombinasikan dengan strap silicone melengkapi tampilan casual pada jam tangan ini.', 81, 5775000, '2020-01-09 21:44:49', '2020-01-09 21:44:49'),
(15, 2, 'FOSSIL – Neutra Chonograph Amber Leather (FS5512)', 'jam10.png', 'Hadir dengan diameter case 44 mm, dilengkapi dengan dial abu abu dan indeks stik, movement kronograf, dan strap kulit berwarna coklat. Jam tangan dengan rating kedalaman 5 ATM dapat dipakai disekitar tempat cuci piring rumah tangga atau saat berenang di perairan dangkal. Jangan gunakan untuk snorkeling atau menyelam.', 60, 2685000, '2020-01-09 21:45:33', '2020-01-09 21:45:33'),
(16, 1, 'Assd', 'jam7.png', 'hghjkkjh', 35, 3000000, '2020-01-10 23:57:18', '2020-01-10 23:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2019-11-01 00:06:34', '2019-11-01 00:52:24'),
(2, 'Custommer', '2019-11-01 00:16:41', '2019-11-18 06:20:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_roles` int(11) NOT NULL DEFAULT '2',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `id_roles`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 1, 'admin@admin.com', NULL, '$2y$10$CgHBPKo9ApsBg4V2bBFhbOzG09bNmkw.tiS8MgbpWZrUa7DxCZNOi', NULL, '2019-10-31 23:37:17', '2019-10-31 23:37:17'),
(7, 'Alif', 2, 'alifsaputra137@gmail.com', NULL, '$2y$10$Y4VPi6TW4CI5lgwbgrP.QO1DoL4u55rVMeEeAHyCdqkkVzdKIggFq', NULL, '2020-01-09 05:21:41', '2020-01-09 05:21:41'),
(8, 'sofi', 2, 'sofi@gmail.com', NULL, '$2y$10$r5fFNI4N5vZOvGbRTd8xgehncFcHWfAQBGRqTs2p5uPts.ZxvBjdm', NULL, '2020-01-09 19:42:23', '2020-01-09 19:42:23'),
(9, 'shishi', 2, 'shishi@gmail.com', NULL, '$2y$10$vDQm.TuCEWtlyIddmF/Rve6vMcwH/F6KV.Uwd8I7CZwUgH0uppyTC', NULL, '2020-01-10 23:27:56', '2020-01-10 23:27:56');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_users` int(11) NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `id_users`, `address`, `phone`, `city`, `postal`, `created_at`, `updated_at`) VALUES
(1, 7, NULL, NULL, NULL, NULL, '2020-01-09 05:21:41', '2020-01-09 05:21:41'),
(2, 8, 'Jakarta', '081289326748', 'Jakarta Timur', '13278', '2020-01-09 19:42:23', '2020-01-09 19:42:55'),
(3, 9, 'Bekasi', '08435679753', 'Bekasi utara', '13245', '2020-01-10 23:27:57', '2020-01-10 23:36:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_providers`
--
ALTER TABLE `oauth_providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_providers_user_id_foreign` (`user_id`),
  ADD KEY `oauth_providers_provider_user_id_index` (`provider_user_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_providers`
--
ALTER TABLE `oauth_providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `oauth_providers`
--
ALTER TABLE `oauth_providers`
  ADD CONSTRAINT `oauth_providers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
