<?php

use Illuminate\Http\Request;
use \App\user_profile;
// use DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/ 

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});











// Api Role

Route::group(["prefix"=>"/roles"], function(){
    Route::get("/all", "RoleController@all");
    Route::post("/store", "RoleController@store");
    Route::delete("/delete/{id}", "RoleController@delete");
    Route::get("/data/{id}", "RoleController@data");
    Route::put("/update/{id}", "RoleController@update");
}); 

Route::group(["prefix"=>"/categories"], function(){
    Route::get("/all", "CategoryController@all");
    Route::post("/store", "CategoryController@store");
    Route::delete("/delete/{id}", "CategoryController@delete");
    Route::get("/data/{id}", "CategoryController@data");
    Route::post("/update", "CategoryController@update");
}); 

Route::group(["prefix"=>"/products"], function(){
    Route::get("/all", "ProductController@all");
    Route::get("/all/paginate", "ProductController@allPaginate");
    Route::post("/search", "ProductController@search");
    Route::post("/store", "ProductController@store");
    Route::post("/filterCondition", "ProductController@filterCondition");
    Route::post("/searchCategory", "ProductController@searchCategory");
    Route::delete("/delete/{id}", "ProductController@delete");
    Route::get("/data/{id}", "ProductController@data");
}); 
Route::group(["prefix"=>"/testimonies"], function(){
    Route::get("/all", "TestimonyController@all");
    Route::get("/all/publish", "TestimonyController@allPublish");
    Route::get("/all/paginate", "TestimonyController@allPaginate");
    Route::post("/search", "TestimonyController@search");
    Route::post("/store", "TestimonyController@store");
    Route::post("/filterCondition", "TestimonyController@filterCondition");
    Route::post("/searchCategory", "TestimonyController@searchCategory");
    Route::delete("/delete/{id}", "TestimonyController@delete");
    Route::get("/data/{id}", "TestimonyController@data");
}); 

Route::group(["prefix"=>"/orders"], function(){
    Route::get("/getAllOrder", "OrderController@getAllOrder");
    Route::get("/orderDetail/{id}", "OrderController@orderDetail");
    Route::get("/confirm/{id}", "OrderController@confirm");
    Route::post("/searchAddress", "OrderController@searchAddress");
    Route::get("/send/{id}", "OrderController@send");
    Route::get("/done/{id}", "OrderController@done");
    Route::delete("/delete/{id}", "OrderController@delete");
});

Route::group(["prefix"=>"/welcome"], function(){
    Route::get("/all", "WelcomeController@all");
});



// Api Frontend Profile

Route::get('/getOrders/{id}', function ($id) {
    return \App\Order::where('user_id', $id)->get();
});

Route::get('/order_detail/{id}', function ( $id) {
    $data['pesan'] = ['pesan' => 'sukses'];
    $data['order'] = \App\OrderItems::leftJoin('products', 'order_items.product_id', '=', 'products.id')->select('order_items.*', 'products.product_name', 'products.product_image')->where(
        'order_items.order_id',
        '=',
        $id
    )->get();
    $data['payment'] = \App\Order::find($id);
    return $data;
});

Route::get('/get_data_user/{id}', function ($id) {
    $user = \App\User::leftJoin('user_profiles', 'user_profiles.id_users', 'users.id')->where('id_users', $id)->select('user_profiles.*', 'users.email', 'users.name')->first();
    return $user;
});

Route::post('/checkTestimony' , function(Request $request){
    $testimony = \App\Testimony::where('name' , $request->name)->where('product_id' , $request->product_id)->first();
    return $testimony;
});

Route::post('/updateUserInformation', function (Request $request) {
        $data = user_profile::where('id_users', $request->id)->first();
        if($data){
            $data->city = $request->city;
            $data->address = $request->address;
            $data->postal = $request->postal;
            $data->phone = $request->phone;
            $data->save();
        }else{
            $profile = new user_profile();
            $profile->id_users = $request->id;
            $profile->city = $request->city;
            $profile->address = $request->address;
            $profile->postal = $request->postal;
            $profile->phone = $request->phone;
            $profile->save();
        }
        return 'sukses';
});


Route::post('/paymentUpload', function (Request $req) {
    $data = \App\Order::find($req->id);
    if($req->file('file')){
        $fileGet = $req->file('file');
        $fileName = $fileGet->getClientOriginalName();
        $move = $fileGet->move('uploads/payment', $fileName);
        $data->payment = $fileName;
    }
    $data->save();
    return $req->file('file')->getClientOriginalName();
});
Route::post('/checkout', function (Request $request) {
    $user = \App\user_profile::where('id_users', Auth::user()->id)->first();

    $order = new \App\Order;
    $order->user_id = Auth::user()->id;
    $order->address = $request->address . ', ' . $request->postal . ', ' . $request->city;
    $order->status = 0;
    $order->total = $request->total;
    $order->save();

    $order_id = \App\Order::orderBy('id', 'DESC')->where('user_id', Auth::user()->id)->first();
    $cart = $request->cart;
    foreach ($cart as $item) :
        $order_detail = array(
            'order_id' => $order_id->id,
            'product_id' => $item['id'],
            'quantity' => $item['quantity'],
            'price' => $item['totalPrice']
        );
        \App\OrderItems::create($order_detail);
    endforeach;
    return $request->cart;
});

Route::get('/deletePayment/{id}', function ($id) {
    $detail = \App\Order::find($id);
    $detail->payment = null;
    $detail->save();
    return $detail;
});