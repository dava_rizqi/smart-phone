<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/  

Route::get("admin", "HomeController@dashboard")->name('dashboard');
Route::resource('admin/roles', 'RoleController');
Route::resource("admin/users", "UserController");
Route::resource('admin/categories', 'CategoryController');
Route::resource('admin/products', 'ProductController');
Route::resource('admin/testimonies', 'TestimonyController');
Route::resource('admin/orders', 'OrderController');

Route::get('{path}', function () {
    return view('index');
})->where('path', '(.*)');
