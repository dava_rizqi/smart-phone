function page(path) {
  return () => import( /* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [

  {
    path: '/login',
    name: 'login',
    component: page('auth/login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: page('auth/register.vue')
  },
  {
    path: '/password/reset',
    name: 'password.request',
    component: page('auth/password/email.vue')
  },
  {
    path: '/password/reset/:token',
    name: 'password.reset',
    component: page('auth/password/reset.vue')
  },
  {
    path: '/email/verify/:id',
    name: 'verification.verify',
    component: page('auth/verification/verify.vue')
  },
  {
    path: '/email/resend',
    name: 'verification.resend',
    component: page('auth/verification/resend.vue')
  },

  {
    path: '/',
    name: 'welcome',
    component: page('home.vue')
  },
  {
    path: '/home',
    name: 'home',
    component: page('home.vue')
  },
  {
    path: '/faq',
    name: 'faq',
    component: page('faq.vue')
  },
  {
    path: '/testimony/:product_id',
    name: 'testimony',
    component: page('testimony.vue')
  },
  {
    path: '/settings',
    component: page('settings/index.vue'),
    children: [{
      path: '',
      redirect: {
        name: 'settings.profile'
      }
    },
    {
      path: 'profile',
      name: 'settings.profile',
      component: page('settings/profile.vue')
    },
    {
      path: 'password',
      name: 'settings.password',
      component: page('settings/password.vue')
    },
    {
      path: 'information',
      name: 'information',
      component: page('settings/information.vue')
    },

    {
      path: 'order_detail/:order_id',
      name: 'order_detail',
      component: page('settings/order_detail.vue')
    },
    {
      path: 'order',
      name: 'order',
      component: page('settings/order.vue')
    }
    ]
  },

  {
    path: '*',
    component: page('errors/404.vue')
  },

  {
    path: '/detail/:product_id',
    name: 'detail',
    component: page('detail.vue')
  },
  {
    path: '/products',
    name: 'products',
    component: page('products.vue')
  },
  {
    path: '/contact',
    name: 'contact',
    component: page('contact.vue')
  },
  {
    path: '/cart',
    name: 'cart',
    component: page('cart.vue')
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: page('checkout.vue')
  },

]
