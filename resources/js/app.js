import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'
import axios from 'axios'
import '~/plugins'
import '~/components'
import VueExpandableImage from 'vue-expandable-image'
import Autocomplete from '@trevoreyre/autocomplete-vue'
import '@trevoreyre/autocomplete-vue/dist/style.css'

Vue.config.productionTip = false
axios.defaults.baseURL = 'http://127.0.0.1:8000/'
/* eslint-disable no-new */
Vue.use(Autocomplete)
Vue.use(VueExpandableImage)
new Vue({
  i18n,
  store,
  router,
  ...App
})
