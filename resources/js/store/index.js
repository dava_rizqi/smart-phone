import Vue from 'vue'
import Vuex from 'vuex'
import Store from './modules/store'
Vue.use(Vuex)

// Load store modules dynamically.
let cart = window.localStorage.getItem('cart');
let cartCount = window.localStorage.getItem('cartCount');
const requireContext = require.context('./modules', false, /.*\.js$/)

const modules = requireContext.keys()
  .map(file => [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)])
  .reduce((modules, [name, module]) => {
    if (module.namespaced === undefined) {
      module.namespaced = true
    }

    return {
      ...modules,
      [name]: module
    }
  }, {})

const store = new Vuex.Store({
  namespaced: true, // Add this here
  modules,
  state: {
    cart: cart ? JSON.parse(cart) : [],
    cartCount: cartCount ? parseInt(cartCount) : 0,
  },
  mutations: {
    addToCart(state, item) {
      let found = state.cart.find(product => product.id == item.id);

      if (found) {
        found.quantity++;
        found.totalPrice = found.quantity * found.product_price;
      } else {
        state.cart.push(item);

        Vue.set(item, 'quantity', 1);
        Vue.set(item, 'totalPrice', item.product_price);
      }

      state.cartCount++;
      this.commit('saveCart');
    },
    saveCart(state) {
      window.localStorage.setItem('cart', JSON.stringify(state.cart));
      window.localStorage.setItem('cartCount', state.cartCount);
    },
    removeFromCart(state, item) {
      let index = state.cart.indexOf(item);

      if (index > -1) {
        let product = state.cart[index];
        state.cartCount -= product.quantity;

        state.cart.splice(index, 1);
      }
      window.localStorage.removeItem(item.id);
    },
    destroyCart(state){
      console.log('ancurin cart')
      state.cartCount = 0
      state.cart = []
      window.localStorage.removeItem('cart');
      window.localStorage.removeItem('cartCount');
    }
  }
});

export default store;
