@php
$config = [
'appName' => config('app.name'),
'locale' => $locale = app()->getLocale(),
'locales' => config('app.locales'),
'githubAuth' => config('services.github.client_id'),
];
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>{{ config('app.name') }}</title>

  <meta name="description" content="">

  <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}">

  {{-- <link rel="manifest" href="site.webmanifest"> --}}
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('vendor/frontend')}}/img/favicon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- CSS here -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/owl.carousel.min.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/animate.min.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/magnific-popup.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/flaticon.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/meanmenu.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/ionicons.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/icomoon.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/slick.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/default.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/style.css">
  <link rel="stylesheet" href="{{asset('vendor/frontend')}}/css/responsive.css">
</head>

<body>
  <div id="app"></div>

  {{-- Global configuration object --}}
  <script>
    window.config = @json($config);
  </script>

  {{-- Load the application scripts --}}
  <script src="{{ mix('dist/js/app.js') }}"></script>

  <!-- JS here -->
  <script src="{{asset('vendor/frontend')}}/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="{{asset('vendor/frontend')}}/js/owl.carousel.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/isotope.pkgd.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/one-page-nav-min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/slick.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/jquery.meanmenu.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/ajax-form.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/wow.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/jquery.scrollUp.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/jquery.final-countdown.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/imagesloaded.pkgd.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/jquery.magnific-popup.min.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/plugins.js"></script>
  <script src="{{asset('vendor/frontend')}}/js/main.js"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.js" integrity="sha512-th3I8avnzF4BCwrfylow7+VRKCXRtC5gsZA8G4ZqaGyhFzNz7BtmPcZs/qjBv0qVRhPAlqzGwkZAliB0AAQbWQ==" crossorigin="anonymous"></script>
</body>

</html>