@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Data Users</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dasbor</a></li>
                    <li class="breadcrumb-item">User</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Dynamic Table Full -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Dynamic Table <small>Full</small></h3>
            <div class="d-flex">
                <form>
                    <div class="input-group">
                        <div class="input-group-btn">
                            <a href="{{route('users.create')}}" class="btn btn-success"><i
                                class="fas fa-plus-circle"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th><b>Name</b></th>
                        <th><b>Email</b></th>
                        <th><b>Role</b></th>
                        <th><b>Action</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $item => $user)
                    <tr>
                        <td class="text-center">{{$item + 1}}</td>
                        <td class="d-none d-sm-table-cell">{{$user->name}}</td>
                        <td class="d-none d-sm-table-cell">{{$user->email}}</td>
                        <td class="d-none d-sm-table-cell">{{$user->id_roles == 1? 'Admin' : 'User'}}</td>
                        <td class="d-none d-sm-table-cell">
                            <a class="btn btn-info text-white btn-sm" href="{{route('users.edit', $user->id)}}">Edit</a>
                            <form onsubmit="return confirm('Delete this user permanently?')" class="d-inline"
                                action="{{route('users.destroy', ['user' => $user->id ])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">

                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>

@endsection