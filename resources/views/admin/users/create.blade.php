@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Create User</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item">Create User</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <!-- Basic Elements -->

                <div class="row push">
                    <div class="col-lg-12">
                        <label for="name">Name</label>
                        <input value="{{old('name')}}"
                            class="form-control {{$errors->first('name') ? "is-invalid": ""}}" placeholder="Full Name"
                            type="text" name="name" id="name" />
                        <div class="invalid-feedback">
                            {{$errors->first('name')}}
                        </div>
                        <br>

                        <label for="username">Username</label>
                        <input value="{{old('username')}}"
                            class="form-control {{$errors->first('username') ? "is-invalid" : ""}}"
                            placeholder="username" type="text" name="username" id="username" />
                        <div class="invalid-feedback">
                            {{$errors->first('username')}}
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="roles">Roles</label>
                            <select class="form-control" id="roles" name="roles">
                                <option value="#">Please select</option>
                                <option value="0">Administrator</option>
                                <option value="1">Staff</option>
                                <option value="2">Custommer</option>
                            </select>
                        </div>

                        <br>
                        <label for="phone">Phone number</label>
                        <br>
                        <input value="{{old('phone')}}" type="text" name="phone"
                            class="form-control {{$errors->first('phone') ? "is-invalid" : ""}} ">
                        <div class="invalid-feedback">
                            {{$errors->first('phone')}}
                        </div>

                        <br>
                        <label for="address">Address</label>
                        <textarea name="address" id="address"
                            class="form-control {{$errors->first('address') ? "is-invalid" : ""}}">{{old('address')}}</textarea>
                        <div class="invalid-feedback">
                            {{$errors->first('address')}}
                        </div>

                        <br>
                        <label for="avatar">Avatar image</label>
                        <br>
                        <input id="avatar" name="avatar" type="file"
                            class="form-control {{$errors->first('avatar') ? "is-invalid" : ""}}">
                        <div class="invalid-feedback">
                            {{$errors->first('avatar')}}
                        </div>


                        <hr class="my-4">

                        <label for="email">Email</label>
                        <input value="{{old('email')}}"
                            class="form-control {{$errors->first('email') ? "is-invalid" : ""}}"
                            placeholder="user@mail.com" type="text" name="email" id="email" />
                        <div class="invalid-feedback">
                            {{$errors->first('email')}}
                        </div>
                        <br>

                        <label for="password">Password</label>
                        <input class="form-control {{$errors->first('password') ? "is-invalid" : ""}}"
                            placeholder="password" type="password" name="password" id="password" />
                        <div class="invalid-feedback">
                            {{$errors->first('password')}}
                        </div>
                        <br>

                        <label for="password_confirmation">Password Confirmation</label>
                        <input class="form-control {{$errors->first('password_confirmation') ? "is-invalid" : ""}}"
                            placeholder="password confirmation" type="password" name="password_confirmation"
                            id="password_confirmation" />
                        <div class="invalid-feedback">
                            {{$errors->first('password_confirmation')}}
                        </div>
                        <br>

                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection