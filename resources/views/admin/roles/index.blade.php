@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Data Roles</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item">Roles</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="block block-rounded block-bordered">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Dynamic Table <small>Full</small></h3>
                </div>
                <div class="block-content block-content-full">
                    <label>Role name</label><br>
                    <input type="text" class="form-control" value="" v-model="role" name="role_name">
                    <br>
                    <button @click="roles_store" class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <!-- Dynamic Table Full -->
            <div class="block block-rounded block-bordered">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Dynamic Table <small>Full</small></h3>
                </div>

                <div class="block-content block-content-full">
                    <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter ">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 80px;">#</th>
                                <th><b>Role Name</b></th>
                                <th><b>Actions</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="role in roles">
                                <td class="text-center">@{{ role.id }}</td>
                                <td class="d-none d-sm-table-cell">@{{ role.role_name }}</td>
                                <td class="d-none d-sm-table-cell text-center">
                                    <a class="btn btn-info text-white btn-sm"
                                        :href="uri + 'admin/roles/' + role.id + '/edit'">Edit</a>
                                    <a href="#" @click="roles_delete(role.id)" class="btn btn-danger btn-sm">Delete</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Dynamic Table Full -->
        </div>
    </div>


</div>

@endsection

@section('script')

<script>
    var App = new Vue({
        el : '#app',
        data(){
            return{
                uri : 'http://127.0.0.1:8000/',
                role : '',
                roles : {},
            }
        },
        mounted() {
            this.roles_all();
        },
        methods : {
            roles_store(){
                axios.post(this.uri + 'api/roles/store', {role_name : this.role })
                .then(response => {
                    this.roles_all();
                    Swal.fire(
                        'Good job!',
                        'You clicked the button!',
                        'success'
                    )
                }).catch(error => {
                    console.log(error)
                })
            },
            roles_all(){
                axios.get(this.uri + 'api/roles/all').then(response => {
                    this.roles = response.data
                }).catch(error => {
                    console.log(error)
                })
            },
            roles_delete(id){
                Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        axios.delete(this.uri + 'api/roles/delete/' + id).then(response => {
                            this.roles_all();
                            Swal.fire(
                                'Good job!',
                                'You clicked the button!',
                                'success'
                            )
                        }).catch(error => console.log(error))
                        
                    }
                })
            }
        }
    })
</script>

@endsection