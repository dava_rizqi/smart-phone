@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Data Orders</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item">Orders</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Dynamic Table Full -->
    <div class="block block-rounded block-bordered">
            <div class="d-flex mx-4 mt-3 align-content-between justify-content-between">
                <div>
                    <input type="text" @keydown="searchAddress" placeholder="Search User ... " v-model="search" class="form-control ">
                    <div v-if="loader">loading ...</div>
                </div>
                <form class="float-right">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <a class="btn btn-success" :href="uri + 'admin/categories/create'"><i
                                    class="fas fa-plus-circle"></i></a>
                        </div>
                    </div>
                </form>
        </div>

        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th><b>Name</b></th>
                        <th><b>Email</b></th>
                        <th><b>Phone</b></th>
                        <th><b>Address</b></th>
                        <th><b>Payment</b></th>
                        <th><b>Status</b></th>
                        <th><b>Orders</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="order in orders">
                        <td class="text-center">@{{order.id}}</td>
                        <td class="d-none d-sm-table-cell">@{{order.name}}</td>
                        <td class="d-none d-sm-table-cell">@{{order.email}}</td>
                        <td class="d-none d-sm-table-cell">@{{order.phone}}</td>
                        <td class="d-none d-sm-table-cell">@{{order.oad}}</td>
                        <td class="d-none d-sm-table-cell">
                            <div v-if="!order.payment" class="badge badge-danger">Waiting for payment</div>
                            <div v-else>
                                <img :src="uri + 'uploads/payment/' + order.payment" width="80" alt="">
                            </div>

                        </td>
                        <td class="d-none d-sm-table-cell">
                            <a v-if="order.status == 0" href="#" @click="confirm(order.id)"
                            class="btn mt-2 btn-success btn-sm">Confirm</a>
                            <a v-else-if="order.status == 1" href="#" @click="send(order.id)"
                                class="btn mt-2 btn-warning btn-sm">Pending</a>
                            <a v-else-if="order.status == 2" href="#" @click="done(order.id)"
                                class="btn mt-2 btn-primary btn-sm">Finish</a>
                            <div v-else-if="order.status == 3" class="text-success">Done</div>
                        </td>
                        <td><a class="btn btn-info text-white btn-sm" @click="orderDetail(order.id)" href="#"
                                data-toggle="modal" data-target="#modalDetail">Show Detail</a>
                            <!-- Modal -->
                            <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5>Product Name</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Quantity</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Price</h5>
                                                </div>
                                            </div>
                                            <div class="row" v-for="detail in orderDetails">
                                                <div class="col-md-4">
                                                    @{{ detail.product_name }}
                                                </div>
                                                <div class="col-md-4">
                                                    x @{{ detail.quantity }}
                                                </div>
                                                <div class="col-md-4">
                                                    @{{ detail.product_price }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" @click="orders_delete(order.id)" class="btn mt-2 btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>

@endsection


@section('script')

<script>
    var App = new Vue({
    el: '#app',
    data() {
      return {
        uri: 'http://127.0.0.1:8000/',
        orders: {},
        orderDetails: {},
        loader : false,
        search : null
      }
    },
    mounted() {
      this.getAllOrder();
    },
    methods: {
        searchAddress(){
        this.loader = true
        axios.post(this.uri + 'api/orders/searchAddress' , {
            search : this.search
        }).then(response => {
            this.orders = response.data
            this.loader = false
            console.log(response.data)
            }).catch(error => {
                console.log(error)
                this.loader = false
            })
        },
      getAllOrder() {
        axios.get(this.uri + 'api/orders/getAllOrder').then(response => {
          this.orders = response.data
          console.log(response.data)
        }).catch(error => {
          console.log(error)
        })
      },

      orderDetail(id) {
        axios.get(this.uri + 'api/orders/orderDetail/' + id).then(response => {
          this.orderDetails = response.data
          console.log(response.data)
        }).catch(error => {
          console.log(error)
        })
      },

      
        confirm(id){
            Swal.fire({
            title: 'Confirm Order ? ',
            text: "You'll be able to revert this",
            role: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!'
            }).then((result) => {
                if (result.value) {
                    axios.get(this.uri + '/api/orders/confirm/' + id).then(
                        res => {
                            console.log(res)
                            this.getAllOrder()
                        }
                    ).catch(err =>console.log)
                }
            })
        },

        send(id){
            Swal.fire({
            title: 'Send Order ? ',
            text: "You'll be able to revert this",
            role: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!'
            }).then((result) => {
                if (result.value) {
                    axios.get(this.uri + '/api/orders/send/' + id).then(
                        res => {
                            console.log(res)
                            this.getAllOrder()
                        }
                    ).catch(err =>console.log)
                }
            })
        },

        done(id){
            Swal.fire({
            title: 'Done Order ? ',
            text: "You'll be able to revert this",
            role: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!'
            }).then((result) => {
                if (result.value) {
                    axios.get(this.uri + '/api/orders/done/' + id).then(
                        res => {
                            console.log(res)
                            this.getAllOrder()
                        }
                    ).catch(err =>console.log)
                }
            })
        },

        orders_delete(id){
            Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    axios.delete(this.uri + 'api/orders/delete/' + id).then(response => {
                        this.getAllOrder();
                        Swal.fire(
                            'Good job!',
                            'You clicked the button!',
                            'success'
                        )
                    }).catch(error => console.log(error))
                    
                }
            })
        },
    },

  })

</script>

@endsection