<script src="{{asset('vendor/admin')}}/js/dashmix.core.min.js"></script>

<script src="{{asset('vendor/admin')}}/js/dashmix.app.min.js"></script>

<!-- Page JS Plugins -->
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/buttons/dataTables.buttons.min.js"></script>
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/buttons/buttons.print.min.js"></script>
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/buttons/buttons.html5.min.js"></script>
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/buttons/buttons.flash.min.js"></script>
<script src="{{asset('vendor/admin')}}/js/plugins/datatables/buttons/buttons.colVis.min.js"></script>

<!-- Page JS Code -->
<script src="{{asset('vendor/admin')}}/js/pages/be_tables_datatables.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.common.dev.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.js"></script>