@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Data testimonies</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item">testimonies</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Dynamic Table Full -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Dynamic Table <small>Full</small></h3>
            <div class="d-flex">
                <div class="input-group">
                    <div class="input-group-btn">
                        <a href="{{route('testimonies.create')}}" class="btn btn-success"><i
                                class="fas fa-plus-circle"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="block-content block-content-full">

            <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <div style="overflow-x: scroll">
                
            <table class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">#</th>
                        <th><b>Name</b></th>
                        <th><b>Testimony</b></th>
                        <th><b>image</b></th>
                        <th><b>Publish</b></th>
                        <th><b>Action</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(testimony , i) in testimonies" :key="i">
                        <td class="text-center">@{{ testimony.id }}</td>
                        <td class="d-none d-sm-table-cell">@{{ testimony.name }}</td>
                        <td class="d-none d-sm-table-cell">@{{ testimony.testimony }}</td>
                                    <td class="d-none d-sm-table-cell">
                                        <img v-if="testimony.image" width="80"
                                            :src="uri + 'uploads/testimony_images/'+ convert(testimony.image)" alt="">
                                        <div v-else>Not set</div>
                                    </td>
                        <td class="d-none d-sm-table-cell" v-if="testimony.is_publish"><div class="badge badge-success">Published</div></td>
                        <td class="d-none d-sm-table-cell" v-else><div class="badge badge-dark">Not Published</div></td>

                        <td class="d-none d-sm-table-cell">
                            <a class="btn btn-info text-white btn-sm"
                                :href="uri + 'admin/testimonies/' + testimony.id + '/edit'">Edit</a>
                            <a href="#" @click="testimony_delete(testimony.id)" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>

@endsection

@section('script')

<script>
    var App = new Vue({
        el : '#app',
        data(){
            return{
                uri : 'http://127.0.0.1:8000/',
                testimonies : {},
                search : null,
            }
        },
        mounted() {
            this.testimonies_all();
        },
        methods : {
            testimonies_all(){
                axios.get(this.uri + 'api/testimonies/all').then(response => {
                    this.testimonies = response.data
                    console.log(response.data)
                }).catch(error => {
                    alert(error.response.data.message)
                })
            },
            convert(img){
                var image = img.split(',')
                console.log(image)
                return image[0] 
            },
            testimony_delete(id){
                Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        axios.delete(this.uri + 'api/testimonies/delete/' + id).then(response => {
                            this.testimonies_all();
                            Swal.fire(
                                'Good job!',
                                'You clicked the button!',
                                'success'
                            )
                        }).catch(error => console.log(error))
                        
                    }
                })
            }
        }
    })
</script>

@endsection