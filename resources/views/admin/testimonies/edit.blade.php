@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Product</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Product</a></li>
                    <li class="breadcrumb-item">Edit Product</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <form action="{{route('testimonies.update', ['testimony'=>$testimony->id])}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <!-- Basic Elements -->

                <div class="row push">
                    <div class="col-lg-12">

                        <label for="categories">Product</label>
                        <select class="form-control" name="product_id">
                            @foreach ($products as $item)
                            <option @if($testimony->product_id == $item->id) selected @endif
                                value="{{ $item->id }}">{{ $item->product_name }}</option>
                            @endforeach
                        </select>
                        <br>

                        <label for="title">Customer Name</label><br>
                        <input type="text" class="form-control" value="{{$testimony->name}}" name="name"
                            placeholder="Product Name" />
                        <br>

                        <label for="product_image">Product Image</label><br>
                        <small class="text-muted">Current cover</small><br>
                        @if($testimony->image)
                        @php
                            $image = explode(',', $testimony->image);   
                            // var_dump($image);
                        @endphp
                        @foreach ($image as $item)
                            <img src="{{asset('uploads/testimony_images/' . $item)}}" width="96px" />
                        @endforeach
                        @endif
                        <br><br>
                        <input type="file" class="form-control" name="image[]" multiple>
                        <small class="text-muted">Kosongkan jika tidak ingin mengubah cover</small>
                        <br><br>

                        <label for="product_description">Testimony</label> <br>
                        <textarea name="testimony" id="product_description"
                            class="form-control">{{$testimony->testimony}}</textarea>
                        <br>
    
                        <select class="form-control"  name="is_publish">
                            <option value="0">Publish Later</option>
                            <option value="1">Publish Immadiately</option>
                        </select>
                        <br>

                        <button class="btn btn-primary" value="submit">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection