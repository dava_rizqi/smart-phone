@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Create Product</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Product</a></li>
                    <li class="breadcrumb-item">Create Product</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <div class="row push">
                <div class="col-lg-12">

                    <label for="product_categories">Product</label><br>
                    <select class="form-control"  v-model="product_id">
                        <option value="0">Pilih Kategori ...</option>
                        @foreach ($products as $item)
                            <option value="{{$item->id}}" >{{$item->product_name}}</option>
                        @endforeach
                    </select>
                    <br>
                    <label for="product_name">Name</label> <br>
                    <input type="text" class="form-control" value="" placeholder="customer name" v-model="name" >
                    <br>

                    <label for="product_image">Product Image</label>
                    <input type="file" ref="file" class="form-control" multiple @change="onChangeFileUpload()" name="file">
                    <br>

                    <label for="product_description">Testimony</label><br>
                    <textarea name="" class="form-control" placeholder="Write a testimony" v-model="testimony"></textarea>
                    <br>

                    <select class="form-control"  v-model="is_publish">
                        <option value="0">Publish Later</option>
                        <option value="1">Publish Immadiately</option>
                    </select>
                    <br>
                    <button @click="testimony_store" class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    var App = new Vue({
    el: '#app',
        data() {
        return {
            uri: 'http://127.0.0.1:8000/',
            file: [], //UNTUK MENYIMPAN DATA FILE YANG AKAN DIUPLOAD
            product_id: '',
            is_publish: 0,
            testimony: '',
            name: '',
            products : [],
        }
    },
    mounted() {
        this.products_all()
    },
    methods: {
        onChangeFileUpload() {
            this.file = this.$refs.file.files;
            console.log(this.file.length)
        },
        testimony_store() {
            let formData = new FormData();
            formData.append('product_id', this.product_id);
            formData.append('name', this.name);
            formData.append('testimony', this.testimony);
            for( var i = 0; i < this.file.length; i++ ){
                let file = this.file[i];
                console.log(file);
                formData.append('files[' + i + ']', file);
            }
            formData.append('is_publish', this.is_publish);
            axios.post(this.uri + 'api/testimonies/store',
                formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(res => {
                console.log(res),
                this.file = [],
                this.product_id = 0,
                this.is_publish = 0,
                this.file =[],
                this.testimony = '',
                this.name = '',
                Swal.fire(
                    'Good job!',
                    'You clicked the button!',
                    'success'
                )
            })
            .catch(err => alert(err.response.data.message));
        },
        products_all(){
                axios.get(this.uri + 'api/products/all').then(response => {
                    this.products = response.data
                    console.log(response.data)
                }).catch(error => {
                    console.log(error)
                })
            },

        }
    })

</script>

@endsection