@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Create Product</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Product</a></li>
                    <li class="breadcrumb-item">Create Product</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <div class="row push">
                <div class="col-lg-12">

                    <label for="product_categories">Product Category</label><br>
                    <select class="form-control"  v-model="product_category">
                        <option value="0">Pilih Kategori ...</option>
                        @foreach ($categories as $item)
                        <option value="{{$item->id}}">{{$item->category_name}}</option>
                        @endforeach
                    </select>
                    <br>
                    
                    <label for="product_categories">Product Category</label><br>
                    <select class="form-control"  v-model="product_condition">
                        <option value="0">Pilih Kondisi ...</option>
                        <option value="Second Kualitas Terbaik">Second Kualitas Terbaik</option>
                        <option value="Second Populer">Second Populer</option>
                        <option value="Second Dibawah 1 juta">Second Dibawah 1 juta</option>
                    </select>
                    
                    <br>

                    <label for="product_name">Product Name</label> <br>
                    <input type="text" class="form-control" value="" v-model="product_name" >
                    <br>

                    <label for="product_image">Product Image</label>
                    <input type="file" ref="file" class="form-control" multiple @change="onChangeFileUpload()" name="file">
                    <br>

                    <label for="product_description">Product Description</label><br>
                    <textarea name="" class="form-control" v-model="product_description"></textarea>
                    <br>


                    <label for="product_stock">Engineering Test</label><br>
                    <input type="number" class="form-control" min="0" max="10" value="" v-model="product_stock" >
                    <br>

                    <label for="product_price">Product Price</label> <br>
                    <input type="number" class="form-control" value="" v-model="product_price" >
                    <br>

                    <button @click="product_store" class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    var App = new Vue({
    el: '#app',
        data() {
        return {
            uri: 'http://127.0.0.1:8000/',
            categories : {},
            product_name: '',
            file: [], //UNTUK MENYIMPAN DATA FILE YANG AKAN DIUPLOAD
            product_description: '',
            product_category: 0,  
            product_condition: 0,  
            product_stock: '',
            product_price: '',
            products: {},
        }
    },
    mounted() {
        // this.categories_all();
    },
    methods: {
        onChangeFileUpload() {
            this.file = this.$refs.file.files;
            console.log(this.file.length)
        },
        product_store() {
            let formData = new FormData();
            formData.append('product_category', this.product_category);
            formData.append('product_condition', this.product_condition);
            formData.append('product_name', this.product_name);
            
            for( var i = 0; i < this.file.length; i++ ){
                let file = this.file[i];
                console.log(file);
                formData.append('files[' + i + ']', file);
            }
            formData.append('product_description', this.product_description);
            formData.append('product_stock', this.product_stock);
            formData.append('product_price',  this.product_price);
            axios.post(this.uri + 'api/products/store',
                formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(res => {
                console.log(res),
                this.file = [],
                this.product_category = 0,
                this.product_category = 0,
                this.product_name ="",
                this.product_description = '',
                this.product_stock = '',
                this.product_price = '',
                Swal.fire(
                    'Good job!',
                    'You clicked the button!',
                    'success'
                )
            })
            .catch(err => alert(err.response.data.message));
        },
        categories_all() {
            axios.get(this.uri + 'api/categories/all').then(response => {
            this.categories = response.data
            }).catch(error => {
            console.log(error)
            })
        },

    }
    })

</script>

@endsection