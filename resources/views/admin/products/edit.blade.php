@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Edit Product</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Product</a></li>
                    <li class="breadcrumb-item">Edit Product</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <form action="{{route('products.update', ['product'=>$product->id])}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="PUT" name="_method">
                <!-- Basic Elements -->

                <div class="row push">
                    <div class="col-lg-12">

                        <label for="categories">Categories</label>
                        <select class="form-control" name="product_category">
                            @foreach ($categories as $item)
                            <option @if($product->product_category == $item->id) selected @endif
                                value="{{ $item->id }}">{{ $item->category_name }}</option>
                            @endforeach
                        </select>
                        <br>
                        <label for="product_categories">Product Category</label><br>
                        <select class="form-control"  v-model="product_condition">
                            <option value="0">Pilih Kondisi ...</option>
                            <option value="Second Kualitas Terbaik">Second Kualitas Terbaik</option>
                            <option value="Second Populer">Second Populer</option>
                            <option value="Second Dibawah 1 juta">Second Dibawah 1 juta</option>
                        </select>
                        <br>

                        <label for="title">Product Name</label><br>
                        <input type="text" class="form-control" value="{{$product->product_name}}" name="product_name"
                            placeholder="Product Name" />
                        <br>

                        <label for="product_image">Product Image</label><br>
                        <small class="text-muted">Current cover</small><br>
                        @if($product->product_image)
                        @php
                            $image = explode(',',$product->product_image);   
                            // var_dump($image);
                        @endphp
                        @foreach ($image as $item)
                            <img src="{{asset('uploads/product_images/' . $item)}}" width="96px" />
                        @endforeach
                        @endif
                        <br><br>
                        <input type="file" class="form-control" name="product_image[]" multiple>
                        <small class="text-muted">Kosongkan jika tidak ingin mengubah cover</small>
                        <br><br>

                        <label for="product_description">Product Description</label> <br>
                        <textarea name="product_description" id="product_description"
                            class="form-control">{{$product->product_description}}</textarea>
                        <br>

                        <label for="product_stock">Engineering Test</label><br>
                        <input type="number" min="0" max="10" class="form-control" placeholder="product_stock" id="product_stock"
                            name="product_stock" value="{{$product->product_stock}}">
                        <br>

                        <label for="product_price">Product Price</label><br>
                        <input type="text" class="form-control" name="product_price" placeholder="product_price"
                            id="product_price" value="{{$product->product_price}}">
                        <br>

                        <button class="btn btn-primary" value="submit">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection