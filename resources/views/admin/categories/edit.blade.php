@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Update Category</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Category</a></li>
                    <li class="breadcrumb-item">Update Category</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" category="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <div class="row push">
                <div class="col-lg-12">
                    <label>Category name</label> <br>
                    <input type="text" class="form-control" v-model="category_name"
                        :placeholder="category.category_name">
                    <br><br>

                    <label>Category Image</label><br>
                    @if($category->category_image)
                    <span>Current image</span><br>
                    <img src="{{asset('uploads/category_images/'. $category->category_image)}}" width="120px">
                    <br><br>
                    @endif
                    <input type="file" ref="file" class="form-control" @change="onChangeFileUpload()" name="file">
                    <small class="text-muted">Kosongkan jika tidak ingin mengubah gambar</small>
                    <br><br>

                    <button @click="categories_update(category.id)" class="btn btn-primary"
                        type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    var App = new Vue({
        el : '#app',
        data(){
            return{
                uri : 'http://127.0.0.1:8000/',
                file: "", //UNTUK MENYIMPAN DATA FILE YANG AKAN DIUPLOAD
                category_name : '',
                category : {},
                categories : {},
            }
        },
        mounted() {
            this.categories_data();
        },
        methods : {
            onChangeFileUpload() {
                this.file = this.$refs.file.files[0];
            },

            categories_update(id){
                let formData = new FormData();
                formData.append('file', this.file);
                formData.append('category_name',  this.category_name);
                formData.append('id', id);

                axios.post(this.uri + 'api/categories/update', 
                    formData, {
                        headers:{
                            'Content-Type': 'multipart/form-data'
                        }
                }).then(res => {
                    console.log(res);
                    this.categories_data();
                    Swal.fire(
                        'Good job!',
                        'You clicked the button!',
                        'success'
                    )
                }).catch(err => console.log(err))
            },
 
            categories_data(){
                axios.get(this.uri + 'api/categories/data/'+ {{$category->id}}).then(response => {
                    this.category = response.data
                    console.log(this.category.id)
                }).catch(error => {
                    console.log(error)
                })
            },
        }
    })
</script>



@endsection