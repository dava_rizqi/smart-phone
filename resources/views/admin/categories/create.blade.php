@extends('admin.master')
@section('content')

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Category User</h1>
            <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">User</a></li>
                    <li class="breadcrumb-item">Create User</li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    @if(session('status'))
    <div class="alert alert-success d-flex align-items-center" role="alert">
        <div class="flex-00-auto">
            <i class="fa fa-fw fa-check"></i>
        </div>
        <div class="flex-fill ml-3">
            <p class="mb-0">{{session('status')}}</p>
        </div>
    </div>
    @endif
    <!-- Elements -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Please fill out this field</h3>
        </div>
        <div class="block-content">
            <!-- Basic Elements -->

            <div class="row push">
                <div class="col-lg-12">

                    <label>Category name</label><br>
                    <input type="text" class="form-control" value="" v-model="category_name" name="">
                    <br>

                    <label>Category image</label>
                    <input type="file" ref="file" class="form-control" @change="onChangeFileUpload()" name="file">
                    <br>

                    <button @click="categories_store" class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    var App = new Vue({
        el: '#app',
            data() {
            return {
                file: "", //UNTUK MENYIMPAN DATA FILE YANG AKAN DIUPLOAD
                uri: 'http://127.0.0.1:8000/',
                category_name: '',
                categories: {},
            }
        },

        mounted() {
            //
        },
 
        methods: {
            onChangeFileUpload() {
                this.file = this.$refs.file.files[0];
            },

            categories_store() {
                let formData = new FormData();
                formData.append('file', this.file);
                formData.append('category_name',  this.category_name);

                axios.post(this.uri + 'api/categories/store',
                    formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(res => {
                    console.log(res),
                    this.$refs.file.value = "",
                    this.category_name="",
                    Swal.fire(
                        'Good job!',
                        'Kategori Berhasil di Tambah',
                        'success'
                    )
                })
                .catch(err => console.log(err));
            },
        }
    })
</script>

@endsection