<!doctype html>
<html lang="en">

<head>
    @include('admin.partials.head')
</head>

<body>
    <div id="app">
        <div id="page-container"
            class="sidebar-o enable-page-overlay side-scroll page-header-fixed page-header-dark main-content-narrow">
            @include('admin.partials.sidebar')
            @include('admin.partials.header')

            <main id="main-container">
                @yield('content')
            </main>

            @include('admin.partials.footer')
        </div>
    </div>
    @include('admin.partials.scripts')
    @yield('script')
</body>